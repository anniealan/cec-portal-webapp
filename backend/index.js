require('babel-register')({
    presets: ['es2015']
  });
  
  if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config({ path: '.env' });
  }
  
  require('./src/server.js');